import numpy as np
import tt
import copy
import time
from scipy.linalg import eigh
from tt.amen import amen_solve, amen_mv
import os


def lobpcg(A, lam0, x0, tol=1e-6, eps=1e-5, maxiter=50, P=None, rmax=10, nswp=4,
                    rmax_U=None, verb=1, H=None, lam_exact=None, folder_name=None, bmv_method='multifun', mv_method='amen',
                    kickrank_amen_mv=0, nswp_amen_mv=2):
    '''

        This is the LOBPCG solver for finding smallest eigenvalues of A

        Input:

            A: TT matrix
            LAM0: 1D numpy array (initial guess of eigenvalues)
            X0: list of TT tensors (initial guess for eigenfunctions)
            TOL: required tolerance of eigenvalues
            EPS: accuracy of operations in the TT format
            MAXITER: maximum number of global iterations
            P: preconditioner, P = P(x, E, eps, rmax), where x is a TT tensor, E is current array of eigenvalues. If P == None, then no preconditioner is used
            RMAX: truncation rank parameter. After each operation tensors are truncated with this parameter
            RMAX_U: truncation parameter of multiplication U[:, :B] * [x, Pr, H]. If None, then RMAX_U = RMAX
            VERB: if verb=1, accuracy of energy will be printed on each step
            LAM_EXACT: exact eigenvalues of A. If given, then logger will contain additional information about convergence with respect to lam_exact
            FOLDER_NAME: output folder

        Output:

            LAM: 1D numpy array of calculated eigenvalues
            X: list of TT tensors (calculated eigenfunctions)
            LOGGER: dictionary with internal information about the iteration process. Keys:
                'err_lam': list of |lam_{k+1} - lam_{k}| / |lam_{k+1}|, where k is the iteration number
                'err_lam_exact': list of |lam_{k} - lam_exact| / |lam_exact| if lam_exact is not None
                'lam': list of lam_{k}
                'residual': ||A x[i] - lam[i] x||/|lam[i]|
                'time_total': total computation time
    '''

    if P == None:
        def P(x, lam, eps, rmax): return x # Identity preconditioner

    if rmax_U is None:
        rmax_U = copy.copy(rmax)

    if folder_name is not None:
        if not os.path.exists(folder_name):
            os.makedirs(folder_name)
        np.savez(folder_name + '/params_lobpcg',
                 eps=eps, maxiter=maxiter, rmax=rmax, rmax_U=rmax_U,
                 tol=tol, f=x0[0].d, N=x0[0].n, B=len(lam0))

    logger = {}
    logger['err_lam'] = []
    logger['err_lam_exact'] = []
    logger['lam'] = []
    logger['residual'] = []
    logger['time_total'] = []

    start_total_time = time.time()

    B = len(lam0)
    n = x0[0].n

    x = copy.deepcopy(x0)
    lam = copy.deepcopy(lam0)

    def matvec(A, x, eps=eps, kickrank=kickrank_amen_mv, nswp=nswp_amen_mv, verb=0, mv_method=mv_method):
        if mv_method == 'amen':
            if kickrank_amen_mv > 0:
                return amen_mv(A, x, eps, y=x, kickrank=kickrank_amen_mv, nswp=nswp_amen_mv, verb=0)[0].round(0, rmax=rmax)
            else:
                return amen_mv(A, x, eps, y=x, kickrank=kickrank_amen_mv, nswp=nswp_amen_mv, verb=0)[0]
        elif mv_method == 'trunc':
            return tt.matvec(A, x).round(0, rmax=rmax)
        else:
            raise Exception('Not implemented Error')

    Ax = []
    for j in range(B):
        Ax.append(matvec(A, x[j]))

    F = block_bilin(x, Ax)
    lam, U = np.linalg.eigh(F)

    # This strange part is due to the fact that kickrank = 0 and the rank will not increase therefore
    for j in range(B):
        x[j] = increase_rank(x[j], rmax)

    x = block_matvec_left(U, x, 0, rmax=rmax, kickrank=0, nswp=1, method=bmv_method)

    for i in range(maxiter):

        x_new = []
        Pr = []
        Ax = []
        AH = []
        APr = []
        r_norm = []
        for j in range(B):
            Ax_j = matvec(A, x[j])
            r_j = (matvec(A, x[j])- lam[j]*x[j])#.round(eps, rmax=rmax)
            Pr_j = P(r_j, lam, eps, rmax)

            Ax.append(Ax_j)
            Pr.append(Pr_j)
            r_norm.append(r_j.norm())

        for j in range(B):
            APr_j = matvec(A, Pr[j])
            APr.append(APr_j)

            if H is not None:
                AH_j = matvec(A, H[j])
                AH.append(AH_j)

        K11 = block_bilin(x, Ax)
        K12 = block_bilin(x, APr)
        K21 = block_bilin(Pr, Ax)
        K22 = block_bilin(Pr, APr)

        M11 = block_bilin(x, x)
        M12 = block_bilin(x, Pr)
        M21 = block_bilin(Pr, x)
        M22 = block_bilin(Pr, Pr)

        if H is not None:
            K13 = block_bilin(x, AH)
            K23 = block_bilin(Pr, AH)
            K31 = block_bilin(H, Ax)
            K32 = block_bilin(H, APr)
            K33 = block_bilin(H, AH)

            M13 = block_bilin(x, H)
            M23 = block_bilin(Pr, H)
            M31 = block_bilin(H, x)
            M32 = block_bilin(H, Pr)
            M33 = block_bilin(H, H)

        if H is not None:
            K = np.array(np.bmat([[K11, K12, K13], [K21, K22, K23], [K31, K32, K33]]))
            M = np.array(np.bmat([[M11, M12, M13], [M21, M22, M23], [M31, M32, M33]]))
        else:
            K = np.array(np.bmat([[K11, K12], [K21, K22]]))
            M = np.array(np.bmat([[M11, M12], [M21, M22]]))

        lam_new, U = eigh(K, M)
        lam_new = lam_new[:B]

        if H is not None:
            x_new = block_matvec_left(U[:, :B], x+Pr+H, eps, rmax=rmax_U, nswp=nswp, method=bmv_method)
            H = block_matvec_left(U[B:, :B], Pr+H, eps, rmax=rmax_U, nswp=nswp, method=bmv_method)
        else:
            H = block_matvec_left(U[B:, :B], Pr, eps, rmax=rmax_U, nswp=nswp, method=bmv_method)
            x_new = block_matvec_left(U[:, :B], x+Pr, eps, rmax=rmax_U, nswp=nswp, method=bmv_method)

        if rmax < rmax_U:
            for j in range(B):
                x_new[j] = x_new[j].round(eps, rmax=rmax)
                H[j] = H[j].round(eps, rmax=rmax)

        end_total_time = time.time()

        # Log step:
        err_lam = np.abs((lam - lam_new) / lam_new)
        if lam_exact is not None:
            err_lam_exact = np.abs((lam - lam_exact) / lam_exact)
        else:
            err_lam_exact = None

        logger['err_lam'].append(err_lam)
        logger['err_lam_exact'].append(err_lam_exact)
        logger['lam'].append(lam_new)
        logger['residual'].append(r_norm / lam)
        logger['time_total'].append(end_total_time - start_total_time)

        if np.max(err_lam) < tol:
            break

        if verb == 1:
            print('Iteration = {0:2d}, err_eigv_best = {1:.2e}, err_eigv_worst = {2:.2e}'.format(i+1, np.min(err_lam), np.max(err_lam)))

        if folder_name is not None:
            save_tt_list(x_new, folder_name + '/' + 'x_lobpcg')
            #save_tt_list(H, folder_name + '/' + 'H_lobpcg')
            np.save(folder_name + '/' + 'lam_lobpcg', lam)
            np.savez(folder_name + '/' + 'log_lobpcg',
                     err_lam=logger['err_lam'],
                     err_lam_exact=logger['err_lam_exact'],
                     lam=logger['lam'],
                     residual=logger['residual'],
                     time_total=logger['time_total']
            )

        x = copy.deepcopy(x_new)
        lam = copy.deepcopy(lam_new)

        if i == maxiter - 1:
            print 'LOBPCG did not converge with desired tolerance'

    return lam, x, logger


def lobpcg_deflation(A, lam0, x0, tol,
                     lam_exact=None, eps=1e-6, maxiter=5, dir_output=False, mv_method='amen',
                     maxiter_loc=10, P=None, rmax=10, radd=0, verb=1, nswp=1, ratio=0.5):

    if dir_output:
        if not os.path.exists(dir_output):
            os.makedirs(dir_output)
        np.savez(dir_output + '/params_dlobpcg',
                 eps=eps, maxiter=maxiter, maxiter_loc=maxiter_loc, nswp=nswp,
                 rmax=rmax, tol=tol, f=x0[0].d, N=x0[0].n, B=len(lam0))

    x_loc = copy.deepcopy(x0)
    lam_loc = copy.copy(lam0)
    lam_exact_loc = copy.copy(lam_exact)

    B = len(x0)

    logger = {}
    logger['err_lam'] = []
    logger['err_lam_exact'] = []
    logger['lam'] = []
    logger['time_total'] = []
    logger['residual'] = []


    Q = []
    x_res = []
    inds_div_loc = np.array([True]*B)
    lam_conv = []
    num_conv = 0.0

    time_start = time.time()
    for i in range(maxiter):

        lam_loc, x_loc, logger_loc = lobpcg_deflation_loc(A, lam_loc, x_loc, folder_name=None, tol=tol,
                                                          lam_exact=lam_exact_loc, eps=eps, mv_method=mv_method,
                                                          maxiter=maxiter_loc, P=P, Q=Q,
                                                          rmax=rmax, verb=verb, nswp=nswp, ratio=ratio)

        inds_conv_loc = list(np.where(np.array(logger_loc['err_lam'][-1]) < tol)[0])
        inds_div_loc = list(np.where(np.array(logger_loc['err_lam'][-1]) >= tol)[0])
        num_conv += len(inds_conv_loc)
        Q += list(np.array(x_loc)[inds_conv_loc])
        if verb == 1:
            print('{0:d}/{1:d} eigenvalues converged with required tol'.format(int(num_conv), B))

        time_end = time.time()
        for j in range(len(logger_loc['lam'])):
            lam = np.array(lam_conv + list(logger_loc['lam'][j]))
            ind_sort = np.argsort(lam)
            lam = lam[ind_sort]
            err_lam = np.array([0]*len(lam_conv) + list(logger_loc['err_lam'][j]))[ind_sort]
            if lam_exact is not None:
                err_lam_exact = (lam - lam_exact) / lam_exact
                logger['err_lam_exact'].append(err_lam_exact)
            logger['lam'].append(lam)
            logger['err_lam'].append(err_lam)
        logger['time_total'].append(time_end - time_start)

        rmax += radd
        lam_conv += list(lam_loc[inds_conv_loc])
        x_loc = np.array(x_loc)[inds_div_loc]
        lam_loc = np.array(lam_loc)[inds_div_loc]
        if lam_exact is not None:
            lam_exact_loc = lam_exact_loc[inds_div_loc]

        if dir_output:
            print ind_sort, inds_conv_loc, inds_div_loc
            save_tt_list(np.array(Q + x_loc.tolist())[ind_sort], dir_output + '/' + 'x_dlobpcg')
            np.save(dir_output + '/' + 'lam_dlobpcg', lam)
            np.savez(dir_output + '/' + 'log_dlobpcg',
                     err_lam=logger['err_lam'],
                     err_lam_exact=logger['err_lam_exact'],
                     lam=logger['lam'],
                     time_total=logger['time_total']
            )

        if num_conv == B:
            break

        if verb == 1:
            if i is not maxiter - 1:
                print('Deflating converged eigenvalues and restarting LOBPCG...')

    return lam, np.array(Q + x_loc.tolist())[ind_sort], logger


def lobpcg_deflation_loc(A, lam0, x0, tol=1e-6, eps=1e-5, maxiter=50, P=None, rmax=10, nswp=4,
                         verb=1, H=None, Q=False, lam_exact=None, folder_name=None, mv_method='amen',
                         kickrank_amen_mv=0, nswp_amen_mv=1, ratio=0.5):
    '''

        This is the LOBPCG solver for finding smallest eigenvalues of A

        Input:

            A: TT matrix
            LAM0: 1D numpy array (initial guess of eigenvalues)
            X0: list of TT tensors (initial guess for eigenfunctions)
            EPS: accuracy of operations in the TT format
            MAXITER: maximum number of global iterations
            P: preconditioner, P = P(x, E, eps, rmax), where x is a TT tensor, E is current array of eigenvalues. If P == None, then no preconditioner is used
            RMAX: truncation rank parameter. After each operation tensors are truncated with this parameter
            VERB: if verb=1, accuracy of energy will be printed on each step
            LAM_EXACT: exact eigenvalues of A. If given, then logger will contain additional information about convergence with respect to lam_exact
            FOLDER_NAME: output folder

        Output:

            LAM: 1D numpy array of calculated eigenvalues
            X: list of TT tensors (calculated eigenfunctions)
            LOGGER: dictionary with internal information about the iteration process. Keys:
                'err_lam': list of |lam_{k+1} - lam_{k}| / |lam_{k+1}|, where k is the iteration number
                'err_lam_exact': list of |lam_{k} - lam_exact| / |lam_exact| if lam_exact is not None
                'lam': list of lam_{k}
                'residual': ||A x[i] - lam[i] x||/|lam[i]|
                'time_total': total computation time
    '''

    if P == None:
        def P(x, lam, eps, rmax): return x # Identity preconditioner


    if folder_name is not None:
        if not os.path.exists(folder_name):
            os.makedirs(folder_name)

        np.savez(folder_name + '/params_lobpcg',
                 eps=eps, maxiter=maxiter, rmax=rmax,
                 tol=tol, f=x0[0].d, N=x0[0].n, B=len(lam0))

    logger = {}
    logger['err_lam'] = []
    logger['err_lam_exact'] = []
    logger['lam'] = []
    logger['residual'] = []
    logger['time_total'] = []

    start_total_time = time.time()

    B = len(lam0)
    n = x0[0].n

    x = copy.deepcopy(x0)
    lam = copy.deepcopy(lam0)

    def matvec(A, x, eps=eps, kickrank=kickrank_amen_mv, nswp=nswp_amen_mv, verb=0, mv_method=mv_method):
        if mv_method == 'amen':
            if kickrank_amen_mv > 0:
                return amen_mv(A, x, eps, y=x, kickrank=kickrank_amen_mv, nswp=nswp_amen_mv, verb=0)[0].round(0, rmax=rmax)
            else:
                return amen_mv(A, x, eps, y=x, kickrank=kickrank_amen_mv, nswp=nswp_amen_mv, verb=0)[0]
        elif mv_method == 'trunc':
            return tt.matvec(A, x).round(0, rmax=rmax)
        else:
            raise Exception('Not implemented Error')

    Ax = []
    for j in range(B):
        Ax.append(matvec(A, x[j]))

    F = block_bilin(x, Ax)
    lam, U = np.linalg.eigh(F)

    # This strange part is due to the fact that kickrank = 0 and the rank will not increase therefore
    for j in range(B):
        x[j] = increase_rank(x[j], rmax)

    x = block_matvec_left(U, x, 0, rmax=rmax, kickrank=0, nswp=1)

    for i in range(maxiter):

        x_new = []
        Pr = []
        Ax = []
        AH = []
        APr = []
        r_norm = []
        for j in range(B):
            Ax_j = matvec(A, x[j])
            r_j = (matvec(A, x[j]) - lam[j]*x[j])#.round(eps, rmax=rmax)
            Pr_j = P(r_j, lam0[j], eps, rmax)

            Ax.append(Ax_j)
            Pr.append(Pr_j)
            r_norm.append(r_j.norm())

        if Q:
            q = len(Q)
            QPr = [None] * B
            for j in range(B):
                Qcf = []
                for k in range(q):
                    Qcf.append(tt.dot(Q[k], Pr[j]))
                Qcf = np.array(Qcf)#.reshape()
                Pr[j] = tt.multifuncrs2([Pr[j]] + Q, lambda x: x[:, 0] - x[:, 1:].dot(Qcf), eps, y0=Pr[j],  kickrank=0, nswp=2, verb=0)

        for j in range(B):
            APr_j = matvec(A, Pr[j])
            APr.append(APr_j)

            if H is not None:
                AH_j = matvec(A, H[j])
                AH.append(AH_j)

        K11 = block_bilin(x, Ax)
        K12 = block_bilin(x, APr)
        K21 = block_bilin(Pr, Ax)
        K22 = block_bilin(Pr, APr)

        M11 = block_bilin(x, x)
        M12 = block_bilin(x, Pr)
        M21 = block_bilin(Pr, x)
        M22 = block_bilin(Pr, Pr)

        if H is not None:
            K13 = block_bilin(x, AH)
            K23 = block_bilin(Pr, AH)
            K31 = block_bilin(H, Ax)
            K32 = block_bilin(H, APr)
            K33 = block_bilin(H, AH)

            M13 = block_bilin(x, H)
            M23 = block_bilin(Pr, H)
            M31 = block_bilin(H, x)
            M32 = block_bilin(H, Pr)
            M33 = block_bilin(H, H)

        if H is not None:
            K = np.array(np.bmat([[K11, K12, K13], [K21, K22, K23], [K31, K32, K33]]))
            M = np.array(np.bmat([[M11, M12, M13], [M21, M22, M23], [M31, M32, M33]]))
        else:
            K = np.array(np.bmat([[K11, K12], [K21, K22]]))
            M = np.array(np.bmat([[M11, M12], [M21, M22]]))

        lam_new, U = eigh(K, M)
        lam_new = lam_new[:B]

        if H is not None:
            H = block_matvec_left(U[B:, :B], Pr+H, eps, rmax=rmax, nswp=nswp)
            # x_new = block_matvec_left(U[:, :B], x+Pr+H, eps, rmax=rmax, nswp=nswp)
            xB = block_matvec_left(U[:B, :B], x, eps, rmax=rmax, nswp=nswp) #+Pr+H
            x_new = []
            for j in range(B):
                x_new.append((xB[j] + H[j]).round(eps, rmax=rmax))
        else:
            H = block_matvec_left(U[B:, :B], Pr, eps, rmax=rmax, nswp=nswp)
            x_new = block_matvec_left(U[:, :B], x+Pr, eps, rmax=rmax, nswp=nswp)

        end_total_time = time.time()

        # Log step:
        err_lam = np.abs((lam - lam_new) / lam_new)
        if lam_exact is not None:
            err_lam_exact = np.abs((lam - lam_exact) / lam_exact)
        else:
            err_lam_exact = None

        logger['err_lam'].append(err_lam)
        logger['err_lam_exact'].append(err_lam_exact)
        logger['lam'].append(lam_new)
        logger['residual'].append(r_norm / lam)
        logger['time_total'].append(end_total_time - start_total_time)

        ratio_converged = len(err_lam[err_lam<tol]) * 1.0 / B

        if verb == 1:
            print('Iteration = {0:2d}, err_eigv_best = {1:.2e}, err_eigv_worst = {2:.2e}'.format(i+1, np.min(err_lam), np.max(err_lam)))

        if np.max(err_lam) < tol:
            break

        if ratio_converged > 0.4 and B > 4:
            break

        if folder_name is not None:
            save_tt_list(x_new, folder_name + '/' + 'x_lobpcg')
            save_tt_list(H, folder_name + '/' + 'H_lobpcg')
            np.savez(folder_name + '/' + 'log_lobpcg',
                     err_lam=logger['err_lam'],
                     err_lam_exact=logger['err_lam_exact'],
                     lam=logger['lam'],
                     residual=logger['residual'],
                     time_total=logger['time_total']
            )

        x = copy.deepcopy(x_new)
        lam = copy.deepcopy(lam_new)

    #if i == maxiter - 1:
    #    print 'LOBPCG did not converge with desired tolerance'
    #print('{0:2d}% of eigenvalues converged with tol'.format(int(100*ratio_converged)))

    return lam, x, logger


def eig_block_sd(A, lam0, x0, eps=1e-5, maxiter=50, P=None, rmax=10, verb=1, lam_exact=None):

    '''

    This is the steepest descent solver for finding smallest eigenvalues of A.
    Optimal tau is chosen separately for each eigenfunction. A more "blocky" version is in eig_block_sd.

    Input:

        A: TT matrix
        LAM0: 1D numpy array (initial guess of eigenvalues)
        X0: list of TT tensors (initial guess for eigenfunctions)
        EPS: accuracy of operations in the TT format
        MAXITER: maximum number of global iterations
        P: preconditioner, P = P(x, E), where x is a TT tensor, E is current array of eigenvalues
        RMAX: truncation rank parameter. After each operation tensors are truncated with this parameter
        VERB: if verb=1, accuracy of energy will be printed on each step
        LAM_EXACT: exact eigenvalues of A. If given, then logger will contain additional information about convergence with respect to lam_exact

    Output:

        LAM: 1D numpy array of calculated eigenvalues
        X: list of TT tensors (calculated eigenfunctions)
        LOGGER: dictionary with internal information about the iteration process. Keys:
            'err_lam': list of |lam_{k+1} - lam_{k}| / |lam_{k+1}|, where k is the iteration number
            'err_lam_exact': list of |lam_{k} - lam_exact| / |lam_exact| if lam_exact is not None
            'err_x': list of ||x_{k+1} - x_{k}|| / ||x_{k+1}||
            'lam': list of lam_{k}
            'F': list of Fock matrices: x_k^T A x_k
    '''


    if P == None:
        def P(x, lam, eps, rmax): return x # Identity preconditioner

    logger = {}
    logger['err_lam'] = []
    logger['err_lam_exact'] = []
    logger['err_x'] = []
    logger['lam'] = []
    logger['F'] = []

    B = len(lam0)
    n = x0[0].n

    x = copy.deepcopy(x0)
    lam = copy.deepcopy(lam0)
    ind_converged = []
    Q = []

    for i in range(maxiter):

        # Step 1: Calculate x_new = x - tau_opt * P * (Ax - Ex),
        x_new = []
        Pr = []
        Ax = []
        for j in range(B):
            Ax_j = tt.matvec(A, x[j]).round(eps, rmax=rmax)
            r_j = (tt.matvec(A, x[j]) - lam[j]*x[j]).round(eps, rmax=rmax)
            Pr_j = P(r_j, lam, eps, rmax)

            Ax.append(Ax_j)
            Pr.append(Pr_j)

        if Q:
            Pr_new = copy.deepcopy(Pr)
            for j in range(B):
                for q in Q:
                    Pr_new[j] = (Pr_new[j] - tt.dot(Pr[j], q) * q).round(eps, rmax=rmax)
            Pr = copy.deepcopy(Pr_new)

        APr = []
        for j in range(B):
            APr_j = tt.matvec(A, Pr[j]).round(eps, rmax=rmax)
            APr.append(APr_j)

        K11 = block_bilin(x, Ax)
        K12 = block_bilin(x, APr)
        K21 = block_bilin(Pr, Ax)
        K22 = block_bilin(Pr, APr)

        M11 = block_bilin(x, x)
        M12 = block_bilin(x, Pr)
        M21 = block_bilin(Pr, x)
        M22 = block_bilin(Pr, Pr)

        K = np.array(np.bmat([[K11, K12], [K21, K22]]))
        M = np.array(np.bmat([[M11, M12], [M21, M22]]))

        lam_new, U = eigh(K, M)
        U1 = U[:B, :B]
        U2 = U[B:, :B]
        lam_new = lam_new[:B]

        xS = block_matvec_left(U1, x, eps, rmax=rmax)
        PrS = block_matvec_left(U2, Pr, eps, rmax=rmax)
        for j in range(B):
            x_new_j = (xS[j] + PrS[j]).round(eps, rmax=rmax)
            x_new.append(x_new_j)

        # Log step:
        err_lam = np.abs((lam - lam_new) / lam_new)
        err_x = [(x[j] - x_new[j]).norm() / (x_new[j]).norm() for j in range(B)]
        err_x = np.array(err_x)
        if lam_exact is not None:
            err_lam_exact = np.abs((lam - lam_exact) / lam_exact)
        else:
            err_lam_exact = None

        logger['err_lam'].append(err_lam)
        logger['err_lam_exact'].append(err_lam_exact)
        logger['err_x'].append(err_x)
        logger['lam'].append(lam_new)

        if verb == 1:
            print('Iteration = {0:2d}, err_eigv_best = {1:.2e}, err_eigv_worst = {2:.2e}'.format(i+1, np.min(err_lam), np.max(err_lam)))

        x = copy.deepcopy(x_new)
        lam = copy.deepcopy(lam_new)

    return lam, x, logger


def cluster(lam, tol):
    B = len(lam)
    clusters = []
    i = 0
    while i < B:
        j = copy.copy(i)
        cluster = [j]
        while True:
            j += 1
            if j < B:
                if (lam[j] - lam[j-1]) / lam[j] > tol:
                    break
                cluster.append(j)
            else:
                break
        clusters.append(cluster)
        i = copy.copy(j)
    return clusters


def inverse_clustered(A, lam0, x0, tol,
                      cls=None, eps=1e-12, rmax=15, maxiter=10, nswp=3, eps_amen=1e-6,
                      kickrank=0, verb=1, lam_exact=None, folder_name=None,
                      kickrank_amen_mv=0, nswp_amen_mv=1):

    if folder_name is not None:
        if not os.path.exists(folder_name):
            os.makedirs(folder_name)
        np.savez(folder_name + '/params_inverse', cls=cls, nswp=nswp,
                 eps=eps, maxiter=maxiter, rmax=rmax,
                 tol=tol, f=x0[0].d, N=x0[0].n, B=len(lam0))

    logger = {}
    logger['err_lam'] = []
    logger['err_lam_exact'] = []
    logger['lam'] = []
    logger['time_total'] = []

    if cls is None:
        cls = [range(len(lam0))]

    lam_inv = []
    x_inv = []

    start_total_time = time.time()

    for c in cls:
        if lam_exact is not None:
            lam_exact_c = lam_exact[c]
        else:
            lam_exact_c = None

        print 'Eigvals numbers:', c
        lam_c, x_c, logger_c = inverse_iter(A, lam0[c], np.array(x0)[c], tol, eps=eps, shift=lam0[c], rmax=rmax, maxiter=maxiter, nswp=nswp,
                                            kickrank=kickrank, verb=verb, verb_amen=0, lam_exact=lam_exact_c, folder_name=None,
                                            kickrank_amen_mv=kickrank_amen_mv, nswp_amen_mv=nswp_amen_mv, eps_amen=eps_amen)

        end_total_time = time.time()

        logger['err_lam'].append(logger_c['err_lam'])
        logger['err_lam_exact'].append(logger_c['err_lam_exact'])
        logger['lam'].append(logger_c['lam'])
        logger['time_total'].append(end_total_time - start_total_time)

        for i in range(len(c)):
            lam_inv.append(lam_c[i])
        x_inv += list(x_c)

        if folder_name is not None:
            save_tt_list(x_inv, folder_name + '/' + 'x_inverse')
            np.save(folder_name + '/' + 'lam_inverse', lam_inv)
            np.savez(folder_name + '/' + 'log_inverse',
                     err_lam=logger['err_lam'],
                     err_lam_exact=logger['err_lam_exact'],
                     lam=logger['lam'],
                     time_total=logger['time_total']
            )

        print ' '

    return lam_inv, x_inv, logger


def inverse_iter(A, lam0, x0, tol,
                 eps=1e-12, shift=None, rmax=20, maxiter=10, nswp=2, iter_qr=1,
                 kickrank=0, verb=1, verb_amen=0, lam_exact=None, folder_name=None,
                 kickrank_amen_mv=1, nswp_amen_mv=1, eps_amen=1e-6):

    if folder_name is not None:
        if not os.path.exists(folder_name):
            os.makedirs(folder_name)

        np.savez(folder_name + '/params_inverse',
                 eps=eps, maxiter=maxiter, rmax=rmax, kickrank=kickrank, nswp=nswp,
                 tol=tol, f=x0[0].d, N=x0[0].n, B=len(lam0))

    logger = {}
    logger['err_lam'] = []
    logger['err_lam_exact'] = []
    logger['lam'] = []
    logger['time_total'] = []

    start_total_time = time.time()

    B = len(lam0)
    n = x0[0].n

    x = copy.deepcopy(x0)
    lam = copy.deepcopy(lam0)

    def mv(A, x):
        res = amen_mv(A, x, eps, y=x, kickrank=kickrank_amen_mv, nswp=nswp_amen_mv, verb=0)[0]
        if kickrank_amen_mv is not 0:
            res = res.round(eps, rmax=rmax)
        return res

    lam_new = copy.deepcopy(lam0)

    for j in range(B):
            x[j] = increase_rank(x[j], rmax)

    i_qr = 0

    for i in range(maxiter):

        # Step 1: Calculate x_new = (A - shift * I)**(-1) x
        x_new = []

        if shift is None: # Rayleigh iteration
            shift = copy.copy(lam_new)

        for j in range(B):
            A_shifted = A - shift[j] * tt.diag(tt.ones(n))
            x_new_j = amen_solve(A_shifted, x[j], x[j], eps_amen, nswp=nswp, kickrank=kickrank)
            if kickrank is not 0:
                x_new_j = x_new_j.round(eps, rmax=rmax)
            x_new.append(x_new_j)

        i_qr += 1
        if i_qr == iter_qr:
            # Step 2: Orthogonalize psi_new
            x_new, L = block_qr(x_new, eps, rmax=rmax)

            # Step 3: Calculate F = psi_new^T A psi_new
            Ax_new = []
            for j in range(B):
                Ax_new.append(mv(A, x_new[j]))
            F = block_bilin(x_new, Ax_new)

            # Step 4: Diagonalize F:
            lam_new, S = np.linalg.eigh(F)

            # Step 5: Set psi = S psi_new
            x_new = block_matvec_left(S, x_new, eps, rmax=rmax, method='trunc')

            i_qr = 0
        else:
            lam_new = []
            for j in range(B):
                x_new[j] = (1.0 / x_new[j].norm()) * x_new[j]
                lam_new.append(tt.dot(mv(A, x_new[j]), x_new[j]))
            lam_new = np.array(lam_new)

        end_total_time = time.time()

        err_lam = np.abs((lam - lam_new) / lam_new)
        if lam_exact is not None:
            err_lam_exact = np.abs((lam - lam_exact) / lam_exact)
        else:
            err_lam_exact = None

        logger['err_lam'].append(err_lam)
        logger['err_lam_exact'].append(err_lam_exact)
        logger['lam'].append(lam_new)
        logger['time_total'].append(end_total_time - start_total_time)

        x = copy.deepcopy(x_new)
        lam = copy.deepcopy(lam_new)

        if verb == 1:
            print('Iteration = {0:2d}, err_eigv_best = {1:.2e}, err_eigv_worst = {2:.2e}'.format(i+1, np.min(err_lam), np.max(err_lam)))

        if np.max(err_lam) < tol:
            break

        if folder_name is not None:
            save_tt_list(x_new, folder_name + '/' + 'x_inverse')
            np.savez(folder_name + '/' + 'log_inverse',
                     err_lam=logger['err_lam'],
                     err_lam_exact=logger['err_lam_exact'],
                     lam=logger['lam'],
                     time_total=logger['time_total']
            )

        if i == maxiter - 1:
            print 'AMEN inverse iteration did not converge with desired tolerance'

    return lam, x, logger



############################
# 	Auxilliary functions   #
############################


def save_tt_list(a, file_name):
    tt_list = []
    for t in a:
        tt_list.append(tt.tensor.to_list(t))
    np.save(file_name, tt_list)


def load_tt_list(file_name):
    tt_list = np.load(file_name)
    a = []
    for t in tt_list:
        a.append(tt.tensor.from_list(t))
    return a


def block_matvec_right(M, vecs, eps, rmax=99999):

    n, m = M.shape
    if len(vecs) <> m:
        raise Exception('Incorrect matrix size')

    new_vecs = []
    for i in range(n):
        w = None
        for j in range(m):
            w += M[i, j] * vecs[j]
            w = w.round(eps, rmax=rmax)
        new_vecs.append(w)

    return new_vecs


def block_matvec_left(M, vecs, eps, rmax=99999, kickrank=0, nswp=4, method='multifun'):

    n, m = M.shape
    if len(vecs) <> n:
        raise Exception('Incorrect matrix size')

    new_vecs = []
    for j in range(m):

        if method == 'multifun':
            w = tt.multifuncrs2(vecs, lambda x: x.dot(M[:, j]), eps, y0=vecs[j],  kickrank=kickrank, nswp=nswp, verb=0)
            if kickrank is not 0:
                w = w.round(eps, rmax=rmax)

        elif method == 'trunc':
            w = None
            for i in range(n):
                w += vecs[i] * M[i, j]
                w = w.round(eps, rmax=rmax)
        new_vecs.append(w)

    return new_vecs


def block_bilin(vecs1, vecs2):

    n = len(vecs1)
    m = len(vecs2)
    b = np.zeros((n, m))

    for i in range(n):
        for j in range(m):
            b[i, j] = tt.dot(vecs1[i], vecs2[j])

    return b


def block_qr(vecs, eps, rmax=99999):

    n = len(vecs)
    g = block_bilin(vecs, vecs)
    L = np.linalg.cholesky(g)

    vecs_orthog = block_matvec_left(np.linalg.inv(L).T, vecs, eps, rmax=rmax, method='trunc')

    return vecs_orthog, L


def increase_rank(a, rmax):
    n = a.n
    d = a.d
    cores = tt.tensor.to_list(a)
    for i in range(d):
        r1, n, r2 = cores[i].shape

        r1_new = max(rmax, r1)
        r2_new = max(rmax, r2)
        if i == 0:
            r1_new = 1
        elif i == d-1:
            r2_new = 1

        new_core = np.zeros((r1_new, n, r2_new))
        new_core[:r1, :, :r2] = copy.copy(cores[i])
        cores[i] = copy.copy(new_core)

    return tt.tensor.from_list(cores)
