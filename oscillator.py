import numpy as np
import tt
import copy

def tt_harmsol(cf, x, B, eps):

    '''
    Finds eigenfunctions of anisotropic harmonic oscillator
        sum_j cf[j] * (-\Delta psi[i] + x**2 psi[i]) = E[i] * psi[i], i=1,..,B

    INPUT
        cf: numpy array of coefficients
        x: numpy array - 1D grid
        B: number of searched eigfuncs
        eps: accuracy of A_harm

    OUTPUT
        psi: list of tt tensors
        E: numpy array
        A_harm: harmonic oscillator operator in the TT format
        xx: list of multidimensional grids in the tt format:
            [x o e o ... o e, e o x o ... o e, ..., e o e o ... o x]
        psi_rand: random rank1 list of orhtogonal tensors
    '''

    N = len(x)
    f = len(cf)

    lp = np.zeros((N, N))
    for i in xrange(N):
        for j in xrange(N):
            if i is not j:
                lp[i,j] = (-1)**(i - j)*(2*(x[i] - x[j])**(-2) - 0.5)
            else:
                lp[i,j] = 1.0/6 * (4*N - 1 - 2*x[i]**2)

    E_1D, psi_1D = np.linalg.eigh(0.5*(lp + np.diag(x**2)))
    E, inds = sort_dD_sum(cf, 0.5*(cf).sum(), B)
    e = tt.eye([N])
    ee = tt.ones([N])


    rand_1d = np.linalg.qr(np.random.random((N, B)))[0]
    psi_rand = []
    for i in range(B):
        ind = inds[i]
        t = tt.tensor(rand_1d[:, ind[0]])
        for j in range(1, f):
            t = tt.kron(t, tt.tensor(rand_1d[:, ind[j]]))
        psi_rand.append(t)

    psi = []
    for i in range(B):
        ind = inds[i]
        t = tt.tensor(psi_1D[:, ind[0]])
        for j in xrange(1, f):
            t = tt.kron(t, tt.tensor(psi_1D[:, ind[j]]))
        psi.append(t)

    lp = tt.matrix(lp)
    lp_f = None
    for i in xrange(f):
        w = cf[i] * lp
        for j in xrange(i):
            w = tt.kron(e, w)
        for j in xrange(i+1, f):
            w = tt.kron(w,e)
        lp_f = lp_f + w
        lp_f = lp_f.round(eps)

    xx = []
    t = tt.tensor(x)
    for  i in xrange(f):
        t0 = t
        for j in xrange(i):
            t0 = tt.kron(ee, t0)
        for j in xrange(i+1, f):
            t0 = tt.kron(t0, ee)
        xx.append(t0)

    harm = None
    for i in xrange(f):
        harm = harm + cf[i] * (xx[i]*xx[i])
        harm = harm.round(eps)

    V_harm = tt.diag(0.5*harm)
    A_harm = 0.5*lp_f + V_harm

    #for i in range(B):
    #     for j in range(B):
        #print (tt.matvec(A_harm, psi[i]) - E[i] * psi[i]).norm(), i,j

    return psi, E, 0.5*lp_f, V_harm, xx, psi_rand, inds


def sort_dD_sum(a, const, N):
    '''
        Finds first N elemens of a sum:
            const + a[0] * n_0 + a[1] * n_1 + ... + a[d] * n_d, where n_i = 0, 1, 2, ...
    '''

    d = a.size
    V = set([tuple(np.zeros(d))])
    ans = np.zeros((N, d))
    values = np.zeros(N)
    for iter_idx in range(1, N):
        best_val = float("+inf")
        for i in range(iter_idx):
            for j in range(d):
                curr_seq = ans[i, :].copy()
                curr_seq[j] += 1
                if tuple(curr_seq) not in V:
                    curr_val = values[i] + a[j]
                    if curr_val < best_val:
                        best_seq = curr_seq
                        best_val = curr_val
                    break
        ans[iter_idx, :] = best_seq
        values[iter_idx] = best_val
        V.add(tuple(best_seq))

    return values + const, np.array(ans, dtype=int)


def tt_bcoupled_energies(omega, alpha, B):

    f = len(omega)
    nu_matr = np.diag(omega**2)
    for i in range(f):
        for j in range(f):
            if i is not j:
                nu_matr[i, j] = alpha[i, j] * np.sqrt(omega[i] * omega[j])
    nu, _ = np.linalg.eigh(nu_matr)
    nu = np.sqrt(nu)
    E, inds = sort_dD_sum(nu, 0.5*nu.sum(), B)

    return E


def tt_bcoupled_pot(alpha, xx, eps):

    V = None
    f = len(xx)

    for i in range(f):
        for j in range(i+1, f):
            V = V + alpha[i, j] * xx[i]*xx[j]
            V = V.round(eps)

    return V


def tt_hh_pot(xx, eps, lm=0.111803):

    V = None
    f = len(xx)

    for s in range(f-1):
        V += (xx[s]*xx[s]*xx[s+1] - (1.0/3)*xx[s+1]*xx[s+1]*xx[s+1])

    return lm*V


def eigb_to_list(y):
    y_list = tt.tensor.to_list(y)
    d = y.d
    first_cores = y_list[:-1]
    y_last_core = y_list[-1]
    n, m, B = y_last_core.shape

    res = []
    for i in range(B):
        #print i
        #print y_last_core[:, :, i].reshape([n, m, 1], order='f').shape
        first_cores.append(y_last_core[:, :, i].reshape([n, m, 1], order='f'))
        res.append(tt.tensor.from_list(first_cores))
        first_cores = first_cores[:-1]

    return res
