#This is a clone of the MATLAB spectral discretization for the Henon-Heiles potential
#Using the Hermite-DVR representation
#The goal is to compute many eigenfunctions of this operator
import numpy as np
from numpy import kron, eye
from numpy.fft import fft
from scipy.linalg import expm
from tt.ksl import ksl
import tt
import time
from math import pi,sqrt
import quadgauss
import os
import sys
import mctdh
import dvr

f = 1 #The number of degrees of freedom
#lm = 0.111803 #The magic constant
lm = 0 #The magic constant

#lm = 
N = 50 # The size of the spectral discretization
trafo, x, dif2, dif1 = mctdh.initho(N, 1.0, 0.0, 0.0, 0.0, '')
basis = 1 # HO Basis
rpbaspar = np.zeros(3, dtype = np.int)
rpbaspar[0] = 0.0 #hoxeq
rpbaspar[1] = 1.0 #hofreq = rpbaspar(2) #ho
rpbaspar[2] = 1.0 #homass = rpbaspar(3) #homass
ipbaspar = []
ipbaspar1 = []
lsq = False
lnw = False
weight = dvr.dvrweights(trafo, x, basis, rpbaspar, ipbaspar, ipbaspar1, lsq, lnw)

lp = -dif2
e = eye(N)

#Calculate the kinetic energy (Laplace) operator
lp2 = None
eps = 1e-8
for i in xrange(f):
    w = lp
    for j in xrange(i):
        w = kron(e,w)
    for j in xrange(i+1,f):
        w = kron(w,e)
    if lp2 is not None:
        lp2 = lp2 + w
    else:
        lp2 = w


#Now we will compute Henon-Heiles stuff
xx = []
t = x
ee = np.ones([N])
for  i in xrange(f):
    t0 = t
    for j in xrange(i):
        t0 = np.kron(ee,t0)
    for j in xrange(i+1,f):
        t0 = np.kron(t0,ee)
    xx.append(t0)

#Harmonic potential
harm = None
for i in xrange(f):
    if harm is not None:
        harm = harm + (xx[i]*xx[i])
    else:
        harm = xx[i] * xx[i]

import aux

z = aux.cap(x, -6.0, -0.5, 2, 1) + aux.cap(x, 6.0, -0.5, 2, -1)

A = 0.5 * lp2 + np.diag(0.5 * harm - 1.0/3.0 * 0.11 * t ** 3) #+ np.diag(z)



H = 1j * A
#Generate the initial Gaussian, which is just shifted
#This is how it is defined in MCTDH
#q1      gauss  2.0  0.0  0.7071
x0 = 2
p0 = 0
dx = 1.0/sqrt(2.0)

gs = np.exp(-0.25*((x-x0)/dx)**2) * np.exp(1j * p0 * (x - x0))
#gs = np.exp(-0.5 * (x ** 2)) 
gs = gs * weight
start = gs/np.linalg.norm(gs)
tau = 0.1;
i = 0
t = 0
tf = 20
cf = []
#S = expm(H * tau * 0.5)
S = expm(H * tau) 
y = start
import time
t1 = time.time()
y = np.dot(S, y)

s = np.dot(y, start)
cf = {}
while t <= tf:
    print '%f/%f' % (t,tf)
    cf[round(t,3)] = np.dot(y,start)
    y = np.dot(S, start)
    print np.linalg.norm(y)
    t += tau
#while t <= tf:
#    print '%f/%f' % (t,tf)
#    cf.append(np.dot(y,np.conj(start)))
#    if abs(t - 0.1) <= 1e-6:
#        pass
#        #import ipdb; ipdb.set_trace()
#    y = np.dot(S,y)
#    t += tau
#t2 = time.time()
#print("Elapsed time: %f" % (t2-t1))


